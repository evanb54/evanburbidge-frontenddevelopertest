

angular.module('capsotestApp')
	.factory('CompList', ['$resource', '$q', function($resource, $q){
		var url = "https://s3-eu-west-1.amazonaws.com/capso.test.data/companies.json";
		return $resource(url, {},{
			query: {method: 'GET', params: {}, isArray: false}
		});	
	}])
	.factory('CompDetails', ['$resource', function($resource){
		var url = 'https://s3-eu-west-1.amazonaws.com/capso.test.data/bondmaster.json';
		return $resource(url, {'companyId': '@companyId'}, {
			query:{method:"GET", params:{}, isArray:false}
		});	
	}])