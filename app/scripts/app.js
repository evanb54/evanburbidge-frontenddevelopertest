'use strict';

/**
 * @ngdoc overview
 * @name capsotestApp
 * @description
 * # capsotestApp
 *
 * Main module of the application.
 */
angular.module('capsotestApp', [
	'ui.router',
	'ngAnimate',
	'ngResource',
	'ngSanitize',
	'ngTouch',
	'ui.bootstrap',
	'chart.js'
]).config(function($stateProvider, $urlRouterProvider , $resourceProvider) {

	$urlRouterProvider.otherwise('/');
	$resourceProvider.defaults.stripTrailingSlashes = false;
	$stateProvider
		.state('companies', {
			url: '/',
			templateUrl: 'views/dashboard.html',
			controller: 'MainCtrl'
		})
		.state('company', {
			url: '/company/:id',
			templateUrl: 'views/company.html',
			controller: 'CompanyCtrl'
		})
		.state('instructions', {
			url: '/instructions',
			templateUrl: 'views/main.html',
			controller: 'MainCtrl'
		})
		.state('improvements', {
			url:'/improvements',
			templateUrl:'views/improvements.html',
			controller:'ImprovementsCtrl'
		});
});
