'use strict';

/**
 * @ngdoc function
 * @name capsotestApp.controller:InstructionsCtrl
 * @description
 * # InstructionsCtrl
 * Controller of the capsotestApp
 */
angular.module('capsotestApp')
  .controller('InstructionsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
