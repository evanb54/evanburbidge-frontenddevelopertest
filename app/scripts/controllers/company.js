'use strict';

/**
 * @ngdoc function
 * @name capsotestApp.controller:CompanyCtrl
 * @description
 * # CompanyCtrl
 * Controller of the capsotestApp
 */
angular.module('capsotestApp')
  .controller('CompanyCtrl', ['CompList','CompDetails', '$scope','$stateParams', function(CompList, CompDetails, $scope, $stateParams){
  	
  	$scope.companyId = parseInt($stateParams.id);
    $scope.bondlimit = 10;
    $scope.bondlimitstart = 0;

  	$scope.compData = {};
  	
  	CompList.query(function(data){
  		$scope.go = true;
  		angular.forEach(data.results, function(item){
  			if($scope.go){
	  			if (item.companyId === $scope.companyId) {
	  				$scope.compData = angular.copy(item);
	  				$scope.go = false;	
	  			}else{
	  				$scope.go = true;
	  			}
	  		}
  		})
  	});

  	$scope.bonddata = [];
  	CompDetails.query({'companyId':$scope.companyId}, function(data){
  		
      var arr = Object.keys(data).map(function(k){return data[k]});

      for(var i = 0; i < 12; i++){
        if ($scope.companyId === arr[i].issuerId) {
          $scope.bonddata.push(arr[i]);
        };
      }

      // Here i'm comparing which values are greater or smaller than the previous values to determine the direction of the bond price.
      for(var j = 0; j < $scope.bonddata.length; j++){
        for(var k = 0; k < 50; k++){
          if ($scope.bonddata[j].prices[k].price < $scope.bonddata[j].prices[k + 1].price) {
            $scope.bonddata[j].prices[k].lessThan = true;
          }else{
            $scope.bonddata[j].prices[k].lessThan = false;
          }
          // console.log($scope.bonddata[j].prices[k].timestamp);

        }
      }
      // future dev on this includes specific table elements being the only ones iterating.
      $scope.nextbonds = function(){
        $scope.bondlimitstart = $scope.bondlimitstart + 11;
      }
      $scope.previousbonds = function(){
        $scope.bondlimitstart = $scope.bondlimitstart - 11; 
      }
  	});
  }]);