'use strict';

/**
 * @ngdoc function
 * @name capsotestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the capsotestApp
 */
angular.module('capsotestApp')
  .controller('MainCtrl', ['$scope', 'CompList',  function($scope, CompList){
    // This variable can be edited to change the color of the graph
    var green = [{
      "fillColor": "rgba(0, 150, 136, 0.3)",
      "strokeColor": "#009688",
      "highlightFill": "rgba(0, 150, 136, 0.3)",
      "highlightStroke": "#009688"
    }];
    // empty objects / arrays to store future data
  	$scope.companies = [];
    $scope.compGraph = {};

    // This function builds the object for chart js.
    var initGraph = function(obj, series, color){
      obj.labels = [];
      obj.data = [];
      obj.series = [series];
      obj.options = {
        datasetFill:true,
        scaleShowHorizontalLines:true
      }
      if (color != null) {
        obj.colors = color;
      };
    }
    // This function pushes all the values into arrays and other parts of the object for chart js.
    function initGraphValues(data, graph, valueField,name){
      var rev = [];
      data.reverse();
      angular.forEach(data, function(item){
        rev.push(item[valueField]);
        graph.labels.push(item[name])
      });
      graph.data.push(rev)
    }
    // gets the min and max prices and returns the difference in a % value.
    var getMinMax = function (value){
      var a = [];
      for(var i = 0; i < value.length; i++){
        a.push(parseInt(value[i].revenue));
      }
      var min = Math.min.apply(null, a),
          max = Math.max.apply(null, a);

          var increase = max - min;
          var diff = increase / max * 100;
          // console.log(diff)
      return diff;
    }
    // gets the total of all revenues
    var getTotal = function(a){
      var total = 0;
      for(var i = 0; i < a.length; i++){
        total += a[i].revenue
      }
      return total;
    }
    // Initalize the graph with the company graph object... compGraph for short
    // This object is attached to the scope so our front end can read its values.
    initGraph($scope.compGraph, 'Revenue', green);
    // Query the service from services.js
    CompList.query(function(item){
      // push the companies into the empty companies array
      $scope.companies = angular.copy(item.results);
      $scope.companiesLen = $scope.companies.length;
      $scope.total = getTotal($scope.companies);
      $scope.difference = getMinMax($scope.companies);
      // give the graph values with the recieved data.
      initGraphValues(item.results, $scope.compGraph, 'revenue', 'legalName');
    });




  }]);