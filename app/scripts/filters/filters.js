
angular.module('capsotestApp')
	.filter('direction', function(){
		return function(input){
			return input ? "glyphicon-chevron-down":"glyphicon-chevron-up";
		}
	});