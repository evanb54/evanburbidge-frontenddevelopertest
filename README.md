# CapSo frontend developer test task #

## Getting started ##

* Use bower and npm to install dependencies
* Run using `grunt serve`
* Go to localhost:9000 and further instructions will be provided there.

CapSo frontend developer submission

Hi, I'm Evan. This is the description of my submission for this task.

I got through 3 of the 4 tasks, the final one proved a bit tricker than i assumed it would be and as such time ran out during my studying of timestamps and how to manipulate them and create ranges based upon the returned values. For the majority of this task I just used the bower components installed with the base however I added additional bower components such as Chart JS and Angular-ui-bootstrap. Some other Angular components I would of liked to of used include lazy loading, and creating a prototype controller creation factory, allows for us not to have to save app.ctrl('ctrl') instead each controller is a subset of a main base "class" controller.

So How did I do it?

**To Get the data...**
I created the file services.js this has two angularjs services in it one to get the companies and another to get the data based upon the company id. These both return the json data as a promise that can then be resolved once we inject the services into the controllers.

**To Handle The Data....**
I've injected each service as a dependency of the controllers that need that data. I then resolve the promise thats returned by those services and set my $scope.variables that I want to output to = the data so that I can use something such as an ng-repeat in dashboard in order to display the data. Another thing I used in here is the CHARt.JS this allows for the nice line graphic displayed on the page

**To create the graph....**
Is kind of annoying, firstly we need to ensure that the object is initialized, it has to have a specific data structure in order to display the chart information. To do this i used the initGraph function in main.js. After this is done the blueprint of our object is created and I use the initGraphValues function in order to populate each value in the object that was initialized in the initGraph function. 

**Other things I did to the data....**
I wanted to put some more ui elements into the overal program so I created functions to get the total of all company revenues, get the difference between the max and min price and return the % of difference and counted how many companies were "subscribed" to the service.

**The specific company data....** 
Was fun to work with, in order to access each object firstly i turned the object full of objects into an array. This allowed me to easily loop through each item in it and compare it the issuerId was equal to the company id and if it was push it to the array that would display data on the page. Once this one done I went into the prices array inside each object and augmented a lessThan value onto it which was either true / false. If true it means that the price is less than the previous one, and false means it was higher. I then sent this value into a filter in filters.js which returned a glyphicon class. This value inserted into a span allowed for a glyphicon to be displayed facing up or down depending on the direction that the price had gone. As there were tons of prices I also created a limit of 10 to ensure that the page didn't run on forever, this state can be changed by clicking the Next / Previous buttons.  One thing for future implementations of this would be only changing specific rows as at the moment it changes them all.

**On how it looks....**
I'm always wanting to ensure that UI is one of the main aspects of a piece that I am working on in addition to ensuring that data is being outputted correctly and manipulated correctly, with this application I thought that the style of an "admin" type interface would work best as it meant that it could also work as a "user portal" in future if the UI was to be further developed. To give a quick visual reference I have attached two screen grabs below.

![Screen Shot 2015-12-08 at 00.20.42.png](https://bitbucket.org/repo/AdK6rn/images/947124302-Screen%20Shot%202015-12-08%20at%2000.20.42.png)
![Screen Shot 2015-12-08 at 00.20.55.png](https://bitbucket.org/repo/AdK6rn/images/4163315328-Screen%20Shot%202015-12-08%20at%2000.20.55.png)

**On its navigation....**
The main state when the application loads is companies. This renders the dashboard.html file and uses the mainCtrl as its controller. the other states include company/:id which takes the id of the clicked on company and inserts it into the url, / instructions was the home page that came with it I kept it there for reference, and /improvements is a url where I kept note of possible improvements.

**What doesn't it do...**
IT doesn't show a list of highest five moving bond prices for a particular time period, this is due to the fact that I did not envision calculating the difference between time a and time b and using that to limit results to be so time consuming or so much of a pain to figure out. In the end time was a factor but it is something that I am going to continue researching so I will know how to do it once I get my head around how the timestamp functionality of javascript works in more detail or find out if angular has any built in methods for dealing with time stamps.

Ways that I could do this include, a similar loop to the lessThan loop in company.js, a recursive function that continuously manipulates an array shifting and unshifting values. But there are many different ways that this function could be done, I intend to find the best way and least complex way it can be done.

thank you for this opportunity.
Evan